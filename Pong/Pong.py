import turtle
import winsound

wn = turtle.Screen()
wn.title("Pong by Me")
wn.bgcolor("black")
wn.setup(width=800, height=600)
wn.tracer(0)

# Score
score_a = 0
score_b = 0

# Paddle A
paddle_a = turtle.Turtle()
paddle_a.speed(0)
paddle_a.shape("square")
paddle_a.color("white")
paddle_a.shapesize(stretch_len=1, stretch_wid=5)
paddle_a.penup()
paddle_a.goto(-350, 0)

# Padle B
paddle_b = turtle.Turtle()
paddle_b.speed(0)
paddle_b.shape("square")
paddle_b.color("white")
paddle_b.shapesize(stretch_len=1, stretch_wid=5)
paddle_b.penup()
paddle_b.goto(+350, 0)

# Ball
ball = turtle.Turtle()
ball.speed(0)
ball.shape("square")
ball.color("white")
ball.penup()
ball.goto(0, 0)
ball.dx = 0.2
ball.dy = 0.2

# Pen

pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write("Player A: 0   Player B: 0", align="center", font=("Courier", 24, "normal"))


# Function for peddle_a
def padlle_a_up():
    y = paddle_a.ycor()
    y += 20
    paddle_a.sety(y)


def padlle_a_down():
    y = paddle_a.ycor()
    y -= 20
    paddle_a.sety(y)


# Keyboard binding for paddle_a
wn.listen()
wn.onkeypress(padlle_a_up, "w")
wn.onkeypress(padlle_a_down, "s")


# Function for peddle_b
def padlle_b_up():
    y = paddle_b.ycor()
    y += 20
    paddle_b.sety(y)


def padlle_b_down():
    y = paddle_b.ycor()
    y -= 20
    paddle_b.sety(y)


# Keyboard binding for paddle_b
wn.listen()
wn.onkeypress(padlle_b_up, "Up")
wn.onkeypress(padlle_b_down, "Down")

# Vain games loop
while True:
    wn.update()

    # Move the ball
    ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)

    # Borders
    if ball.ycor() > 290:  # up border
        ball.sety(290)
        ball.dy *= -1
        winsound.PlaySound("hit ground.wav", winsound.SND_ASYNC)

    if ball.ycor() < -290:  # down border
        ball.sety(-290)
        ball.dy *= -1
        winsound.PlaySound("hit ground.wav", winsound.SND_ASYNC)

    if ball.xcor() > 390:  # right border
        ball.goto(0, 0)
        ball.dx *= -1
        score_a += 1
        pen.clear()
        pen.write("Player A: {}   Player B: {}".format(score_a, score_b), align="center",
                  font=("Courier", 24, "normal"))
        winsound.PlaySound("lose ball.wav", winsound.SND_ASYNC)


    if ball.xcor() < -390:  # left border
        ball.goto(0, 0)
        ball.dx *= -1
        score_b += 1
        pen.clear()
        pen.write("Player A: {}   Player B: {}".format(score_a, score_b), align="center",
                  font=("Courier", 24, "normal"))
        winsound.PlaySound("lose ball.wav", winsound.SND_ASYNC)


    # Paddle and ball collisions
    if (340 < ball.xcor() < 350) and (ball.ycor()) < paddle_b.ycor() + 40 and ball.ycor() > paddle_b.ycor() - 40:
        ball.setx(340)
        ball.dx *= -1
        winsound.PlaySound("1.wav", winsound.SND_ASYNC)

    if (-350 < ball.xcor() < -340) and (ball.ycor()) < paddle_a.ycor() + 40 and ball.ycor() > paddle_a.ycor() - 40:
        ball.setx(-340)
        ball.dx *= -1
        winsound.PlaySound("1.wav", winsound.SND_ASYNC)